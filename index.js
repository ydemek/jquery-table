$(function () {

    var dataTable = $("#dataTable");
    var tableElement;
    var txtBookName;
    var txtBookCategory;
    var txtBookPrice;
    var txtBookDate = new Date();
    var date;
    var txtBookId = 5;
    var myBooks = [
        {
            "bookId": "1",
            "bookName": "The Grass is Always Greener",
            "bookCategory": "Modern Times",
            "bookPrice": "125.60",
            "bookDate": "3/1/2019 11:10"
        },
        {
            "bookId": "2",
            "bookName": "Murder",
            "bookCategory": "Crime",
            "bookPrice": "56.00",
            "bookDate": "9/7/2019 19:50"
        },
        {
            "bookId": "3",
            "bookName": "A Boy at Seven",
            "bookCategory": "Romance",
            "bookPrice": "56.00",
            "bookDate": "7/5/2019 12:21"
        },
        {
            "bookId": "4",
            "bookName": "The Signalman",
            "bookCategory": "Adventure",
            "bookPrice": "56.00",
            "bookDate": "6/2/2019 10:39"
        },
        {
            "bookId": "5",
            "bookName": "Popular Science",
            "bookCategory": "Science",
            "bookPrice": "210.40",
            "bookDate": "7/1/2019 15:40"
        }
    ]
    // myBooks = $.parseJSON(myBooks);

    tableElement = '<table class="table  table-dark table-responsive" >';
    tableElement += "<tr>";
    tableElement += "<th>" +  "Book Id" + "</th>";
    tableElement += "<th>" + "Book Name"  + "</th>";
    tableElement += "<th>" + "Category"  + "</th>";
    tableElement += "<th>" + "Price"  + "</th>";
    tableElement += "<th>" +  "Date" + "</th>";
    tableElement += "</tr>";
    
    $.each(myBooks, function (i, myBook) {
        tableElement += "<tr>";
        tableElement += "<td>" + myBook.bookId + "</td>";
        tableElement += "<td>" + myBook.bookName + "</td>";
        tableElement += "<td>" + myBook.bookCategory + "</td>";
        tableElement += "<td>" + myBook.bookPrice + "</td>";
        tableElement += "<td>" + myBook.bookDate + "</td>";
        tableElement += "</tr>";


    });
    tableElement += "</table>";
   // console.log(tableElement);
    dataTable.append(tableElement);

    $('#btnClick').click(function (event) {
        txtBookId ++;
        txtBookName = $('#txtBookName').val();
        txtBookCategory = $('#txtBookCategory').val();
        txtBookPrice = $('#txtBookPrice').val();
        tableElement = '';
        y =txtBookDate.getFullYear();
        m =txtBookDate.getMonth() + 1;
        d =txtBookDate.getDate();
        h =txtBookDate.getHours();
        c =txtBookDate.getMinutes();
        
       if(c < 10){
        date =  m + "/" + d + "/" + y + " " + h + ":" + "0" + c ;
       }else{
         date =  m + "/" + d + "/" + y + " " + h + ":" + c ;
       }
      

        tableElement += "<tr>";
        tableElement += "<td>" + txtBookId + "</td>";
        tableElement += "<td>" + txtBookName + "</td>";
        tableElement += "<td>" + txtBookCategory + "</td>";
        tableElement += "<td>" + txtBookPrice + "</td>";
        tableElement += "<td>" + date + "</td>";
        tableElement += "</tr>";
        $('.table').append(tableElement);


        if(txtBookId == 10){
            $('#form #btnClick').stop().fadeOut(10);
            $('#form').slideUp(1000);
            $('#dataTable').delay(500).animate({
             
               marginLeft : "+=200px" 
               
               
            },1500);
        }
    });

    $('html').keydown(function(event){
        //console.log(event.which);
        if(event.which == 13){
            $('#btnClick').click();
        }
    });
   
});

